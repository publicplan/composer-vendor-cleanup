<?php
declare(strict_types=1);

namespace publicplan\ComposerVendorCleanup;

use Composer\IO\IOInterface;


class Cleaner
{

    /**
     * @var IOInterface
     */
    private $io;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var bool
     */
    private $matchCase;

    /**
     * @var array
     */
    private $devFiles;

    /**
     * @var bool
     */
    private $removeEmptyDirs;

    /**
     * @var int
     */
    private $packagesCount;

    /**
     * @var int
     */
    private $removedDirectories = 0;

    /**
     * @var int
     */
    private $removedFiles = 0;

    /**
     * @var int
     */
    private $removedEmptyDirectories = 0;

    public function __construct(IOInterface $io, Filesystem $filesystem, array $devFiles)
    {
        $this->io = $io;
        $this->filesystem = $filesystem;
        $this->devFiles = $devFiles;
    }

    /**
     * @param Package[] $packages
     */
    public function cleanupPackages($packages)
    {
        $this->io->write("");
        $this->io->write("Composer vendor cleaner: <info>Cleaning packages in vendor directory</info>");


    }

}
